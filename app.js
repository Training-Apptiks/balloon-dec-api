var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var cakeRoutes = require('./routes/cakes');
var plantRoutes = require('./routes/plants');
var chocoRoutes = require('./routes/chocolates');
var occasionsRoutes = require('./routes/occasions')
var jamRoutes = require('./routes/jam')

var app = express();
var mongoose = require('mongoose');
const { connect } = require('http2');


//mongoose connection

mongoose.connect('mongodb+srv://admin:admin@cluster0.di8rr.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
{
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error :"));
db.once("open", function(){
  console.log("MongoDB connected successfully")
})




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.get('/hello',function(req,res){
  res.send('Hello World!')
});

app.get('/wish',function(req,res){
  res.send('Hello Good Morning !!!')
})

// http methods
// GET
// POST
// PUT
// Options
// Head
// Delete


app.post('/contactInfo',function(req,res){

  console.log("from new ContactIno api")
  res.send(req.body)

})

app.use('/cakes',cakeRoutes); // 1) /cakes/getCake 2) /cake/addCake
app.use('/plants',plantRoutes); // 1) /plants/pl/m1/getPlants 2) /plants/getCategory 3) /plants/addPLants
app.use('/chocolates',chocoRoutes);
app.use('/occasions',occasionsRoutes);
app.use('/jam',jamRoutes)  
app.post('/wish',function(req,res){
  res.send(req.body )
});

app.get('/cake/test/v1/file/m2/getCakes',function(req,res){
  res.send('Hello from /cake/getCakes');
})

app.get('/price/pricise',function(req,res){
  console.log(req.body);
  res.send("From price api");
});

app.get('/count',function(req,res){
  console.log(req.body);
  res.send("From count api");
});

app.put('/makeFriend',function(req,res){
  console.log(req.body);
  res.send(req.body);
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};


  // render the error page
  res.status(err.status || 500);
  res.render('error');

 
});



module.exports = app;
